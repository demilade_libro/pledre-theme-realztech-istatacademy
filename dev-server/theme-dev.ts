// @ts-ignore
import * as http from "http"
// @ts-ignore
import * as fs from "fs"
// @ts-ignore
import * as path from "path"
import { Liquid } from "liquidjs";

// @ts-ignore
const indexHtml = fs.readFileSync(path.resolve(__dirname, '../index.liquid'), 'utf8')


const engine = new Liquid();



const parseTemplate = async (liquidString: string, themeOptions: any) => {
    return engine.parseAndRender(liquidString, themeOptions);
}


const generateHtmlFromIndexLiquid = async () => {
    const schoolDetails = {}
    const schoolDomain = {}
    const additionalThemeOptions = {}
    const courses = []
    const categories = []

    const schoolThemeOptions = {
        category_list: categories,
        course_list: courses,
        school_domain: schoolDomain,
        school_details: schoolDetails,
    };
    return parseTemplate(
        indexHtml,
        {
            ...schoolThemeOptions,
            ...additionalThemeOptions,
        },
    );
};

// @ts-ignore
http.createServer(async function (req, res) {
    const html = await generateHtmlFromIndexLiquid()
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(html)
    // res.end('Hello World!');
}).listen(8080);
